﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TV7.core;
using TV7.core.Log;
using TV7.core.Types;
using TV7.drivers;

namespace TV7
{
    /// <summary>
    /// Класс реализующий опрос прибора.
    /// </summary>
    internal class ReadDriver : IDisposable
    {
        private Logger logger;
        private ReadDeviceData readDeviceData;
        private ArchiveHeader archiveHeader;

        public ReadDriver(Logger logger, ReadDeviceData readDeviceData)
        {
            this.logger = logger;
            this.readDeviceData = readDeviceData;
        }

        public void Dispose()
        {
            readDeviceData.driver.Dispose();
        }

        /// <summary>
        /// Опрос прибора.
        /// </summary>
        public async Task ReadData()
        {
            await Task.Run(() =>
            {
                try
                {
                    logger.Info("Опрос прибора начат.");
                    try
                    {
                        readDeviceData.driver.Init(readDeviceData);
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                        throw new Tv7StopInterview("Ошибка Init.");
                    }
                    try
                    {
                        readDeviceData.driver.ReadDateTime();
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }
                    try
                    {
                        readDeviceData.driver.SetDateTime(DateTime.Now);
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }
                    try
                    {
                        readDeviceData.driver.ReadConfig();
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }
                    try
                    {
                        readDeviceData.driver.ReadCurrentData();
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }
                    try
                    {
                        archiveHeader = readDeviceData.driver.ReadArchiveHeader();
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }
                    try
                    {
                        if (archiveHeader != null)
                        {
                            if (!archiveHeader.isEmpty())
                            {
                                readDeviceData.driver.ReadArchiveData(archiveHeader);
                            }
                            else
                            {
                                throw new TV7ArchiveHeaderIsEmptyException("Архив прибора пустой.");
                            }
                        }
                    }
                    catch (InterfaceExceptions exp)
                    {
                        logger.Error(exp.Message);
                    }

                }
                catch (ModBusExceptions exp)
                {
                    logger.Error(exp.Message);
                }
                catch (ThreadAbortException exp)
                {
                    readDeviceData.driver.Dispose();
                    logger.Info("Опрос остановлен.");
                }
            });
        }
    }
}