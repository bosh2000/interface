﻿using System;
using System.IO.Ports;
using System.Threading;
using TV7.core.Log;

namespace TV7.core
{
    internal class ComPort : IStream, IDisposable
    {
        /// <summary>
        /// Экзепляр класса SerialPort.
        /// </summary>
        private SerialPort port;

        /// <summary>
        /// Экзепляр класса Logger.
        /// </summary>
        private Logger logger;

        /// <summary>
        /// Есть принятые данные .
        /// </summary>
        public bool dataReceived;

        /// <summary>
        /// Массив принятых байт
        /// </summary>
        public byte[] receivedData;

        /// <summary>
        /// Задержка ожидание ответа.
        /// </summary>
        private int delayDataReceive = 4000;

        /// <summary>
        /// Счетчик повторов, при ошибках приема или передачи.
        /// </summary>
        private int count;

        public bool isConnect { get; set; }

        private string deviceComPort;

        /// <summary>
        /// Уставка количество попыток чтения данных из потока.
        /// </summary>
        /// <param name="count"></param>
        public void SetCount(int count)
        {
            this.count = count;
        }

        public ComPort(string deviceComPort)
        {
            this.logger = Logger.GetInstance(); ;
            this.deviceComPort = deviceComPort;
        }

        /// <summary>
        /// Реализация интерфейса IDisposable.
        /// </summary>
        public void Dispose()
        {
            try
            {
                port.Close();
            }
            catch (Exception exp)
            {
                logger.Error(string.Format("Ошибка закрытия порта - {0}", exp.Message));
            }
            logger.Info("Порт закрыт.");
        }

        /// <summary>
        /// Илициализация экземпляра класса и открытие COM порта.
        /// </summary>
        /// <returns></returns>
        public void Init()
        {
            //string ComPortSerail = "COM3";
            SetCount(10);
            port = new SerialPort(deviceComPort);
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.DataBits = 8;
            port.RtsEnable = true;
            port.ReadTimeout = 500;
            port.DataReceived += new SerialDataReceivedEventHandler(DataReceive);
            try
            {
                port.Open();
                isConnect = true;
            }
            catch (Exception exp)
            {
                logger.Error(exp.Message);
                throw new Tv7ComPortOpenExceptions(exp.Message);
            }
            logger.Info(string.Format("Порт {0} открыт, на скорости {1}", deviceComPort, port.BaudRate));
        }

        public Responce ReadResponce()
        {
            throw new NotImplementedException();
        }

        public Request WriteRequest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Делегат, вызывается при получении денных и таймаутом port.ReadTimeout.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DataReceive(object sender, SerialDataReceivedEventArgs e)
        {
            // Вместо задержки перед чтением организовать цикличное чтение по длине ответа.
            Thread.Sleep(500);
            try
            {
                SerialPort sp = (SerialPort)sender;
                int lengthReceivedBytes = sp.BytesToRead;
                byte[] responceBytes = new byte[lengthReceivedBytes];
                sp.Read(responceBytes, 0, lengthReceivedBytes);
                receivedData = responceBytes;
                dataReceived = true;
            }
            catch (InvalidOperationException exp)
            {
                logger.Error($"Ошибка чтения из потока - {exp.Message}");
            }
        }

        public byte[] ReadBytes()
        {
            return receivedData;
        }

        /// <summary>
        // Запись в порт массива байт, с ожиданием ответа.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public void WriteBytes(byte[] bytes)
        {
            //            await Task.Run(() =>
            //          {
            int currentCount = count;
            dataReceived = false;
            receivedData = null;  // Очистка входного буфера.
            port.Write(bytes, 0, bytes.Length);
            logger.DumpSend(bytes, false);
            Thread.Sleep(delayDataReceive);
            while (!dataReceived && currentCount >= 0)
            {
                logger?.Warning("Ответ не получен...");

                Thread.Sleep(delayDataReceive);
                currentCount--;
            };
            if (!dataReceived && currentCount == 0)
            {
                throw new Tv7TimeOutException("За период ожидания, ответ не получен. Остановка опроса.");
            };
            logger.DumpReceive(GetResponce(), false);

            //        });
        }

        /// <summary>
        /// Записть массива байт в порт, без ожидания ответа.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public void WriteBytesNoAnswer(byte[] bytes)
        {
            //await Task.Run(() =>
            //{
            port.Write(bytes, 0, bytes.Length);
            //});
        }

        /// <summary>
        /// Флаг, обозначающий что данные приняты.
        /// </summary>
        /// <returns></returns>
        public bool IsDatareceiverd()
        {
            return dataReceived;
        }

        /// <summary>
        /// Возвращает принятый массив байт.
        /// </summary>
        /// <returns></returns>
        public byte[] GetResponce()
        {
            return receivedData;
        }
    }
}