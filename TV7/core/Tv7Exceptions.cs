﻿using System;

namespace TV7.core
{
    /// <summary>
    /// Класс реализующий внутренние исключения проекта.
    /// </summary>
    internal class InterfaceExceptions : Exception
    {
        public InterfaceExceptions(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, дисконект.
    /// </summary>
    internal class TV7DisconectException : InterfaceExceptions
    {
        public TV7DisconectException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, заголовок архивов пустой.
    /// </summary>
    internal class TV7ArchiveHeaderIsEmptyException : InterfaceExceptions
    {
        public TV7ArchiveHeaderIsEmptyException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, превышение ТаймАут.
    /// </summary>
    internal class Tv7TimeOutException : InterfaceExceptions
    {
        public Tv7TimeOutException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, не реализованно.
    /// </summary>
    internal class Tv7NotImplemented : InterfaceExceptions
    {
        public Tv7NotImplemented(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Остановка опроса изза ошибки данных.
    /// </summary>

    internal class Tv7StopInterviewWinthError : InterfaceExceptions
    {
        public Tv7StopInterviewWinthError(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Ошибка открытия COM порта.
    /// </summary>
    internal class Tv7ComPortOpenExceptions : InterfaceExceptions
    {
        public Tv7ComPortOpenExceptions(string message) : base(message)
        {
        }
    }

    internal class Tv7StopInterview: InterfaceExceptions
    {
        public Tv7StopInterview(string message) : base(message)
        {

        }
    }
}