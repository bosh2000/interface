﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TV7.core.Types
{

    /// <summary>
    /// Структура для записи и чтения регистров, использование функции Modbus 0x48.
    /// </summary>
    public struct WriteReadStruct
    {
        /// <summary>
        /// Начальный адрес для чтения.
        /// </summary>
        public ushort StartReadRegister;

        /// <summary>
        /// Количество регистров для чтения.
        /// </summary>
        public ushort ReadCount;

        /// <summary>
        /// Начальный адрес для записи.
        /// </summary>
        public ushort StartWriteRegister;

        /// <summary>
        /// Количество регистров для записи.
        /// </summary>
        public ushort WriteCount;

        /// <summary>
        /// Массив байт для записи.
        /// </summary>
        public byte[] ToWriteByteArray;
    }

}
