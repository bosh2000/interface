﻿using System;

namespace TV7.core.Types
{
    /// <summary>
    /// Структура описывающая архив, его тип и дату начала и окончания.
    /// </summary>
    internal struct ArchiveDateDistance
    {
        /// <summary>
        /// Тип архива , часовой , суточный и т.д.
        /// </summary>
        public TypeArchive TypeArchive;

        /// <summary>
        /// Дата начала архива.
        /// </summary>
        public DateTime startDateTime;

        /// <summary>
        /// Дата окончания архива.
        /// </summary>
        public DateTime endDateTime;
    }
}