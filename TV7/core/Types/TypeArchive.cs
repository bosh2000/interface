﻿namespace TV7.core.Types
{
    /// <summary>
    /// Тип архивов прибора.
    /// </summary>
    internal enum TypeArchive
    {
        /// <summary>
        /// Часовой архив.
        /// </summary>
        Hour,

        /// <summary>
        /// Суточный архив.
        /// </summary>
        Day,

        /// <summary>
        /// Месячный архив.
        /// </summary>
        Mouth,

        /// <summary>
        /// Текущие данные.
        /// </summary>
        Current,

        /// <summary>
        /// Архив ЧС.
        /// </summary>
        Incidents,

        /// <summary>
        /// Архив ошибок прибора.
        /// </summary>
        Error
    }
}