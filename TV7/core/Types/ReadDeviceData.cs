﻿using System;
using System.Collections.Generic;
using TV7.drivers;
using TV7.core.Log;

namespace TV7.core.Types
{
    /// <summary>
    /// Класс описывающий прибор и данные считываемые с него.
    /// </summary>
    internal class ReadDeviceData
    {
        public int serialNumber { get; set; }
        public int adressDevice { get; set; }
        public List<TypeArchive> ReadArchives { get; set; }
        public DateTime dtStartRead { get; set; }
        public DateTime dtEndRead { get; set; }
        public IDrivers driver;
        public IStream stream;
        public ReportingDateTime reportingDateTime;

        public ReadDeviceData()
        {
            ReadArchives = new List<TypeArchive>();
        }
    }
}