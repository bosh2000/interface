﻿using System.Collections;
using System.Collections.Generic;

namespace TV7.core.Types
{
    /// <summary>
    /// Класс описывающий архивы и диапазон даты архивов.
    /// </summary>
    internal class ArchiveHeader : IEnumerable<ArchiveDateDistance>
    {
        /// <summary>
        /// Есть данные по заголовкам архивов.
        /// </summary>
        private bool empty;

        /// <summary>
        /// Список типов архивов и диапазона дат .
        /// </summary>
        private List<ArchiveDateDistance> archiveDateDistances;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public ArchiveHeader()
        {
            archiveDateDistances = new List<ArchiveDateDistance>();
            this.empty = true;
        }

        /// <summary>
        /// Добавление заголовка архива.
        /// </summary>
        /// <param name="add"></param>
        public void AddArchiveDateDistance(ArchiveDateDistance add)
        {
            archiveDateDistances.Add(add);
            empty = false;
        }

        /// <summary>
        /// Список заколовков пустой, нет архивов для чтения.
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            return empty;
        }

        /// <summary>
        /// Реализация интерфейся IEnumerable
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ArchiveDateDistance> GetEnumerator()
        {
            return (archiveDateDistances.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (archiveDateDistances.GetEnumerator());
        }
    }
}