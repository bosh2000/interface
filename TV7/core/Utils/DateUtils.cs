﻿using System;
using TV7.core.Types;

namespace TV7.core.Utils
{
    internal static class DateUtils
    {
        /// <summary>
        /// Класс для пребразования Даты\Время в тип DateTime.
        /// </summary>
        /// <param name="rsp">Массив байт, (ответ прибора).</param>
        /// <returns>Дата в формате DateTime.</returns>
        public static DateTime GetDateTime(Responce rsp, int offSet)
        {
            int dayMount = Util.GetInt16(rsp.GetResponceBody(), offSet);
            int yearHour = Util.GetInt16(rsp.GetResponceBody(), offSet + 2);
            int minSec = Util.GetInt16(rsp.GetResponceBody(), offSet + 4);
            int day = 0b0000000011111111 & dayMount;
            int mount = dayMount >> 8;
            int year = 0b0000000011111111 & yearHour;
            int hour = yearHour >> 8;
            int min = 0b0000000011111111 & minSec;
            int sec = minSec >> 8;
            return new DateTime(year + 2000, mount, day, hour, min, sec);
        }

        /// <summary>
        /// Установка даты и времени в формате прибора.
        /// </summary>
        /// <param name="dt">Дата для установки.</param>
        /// <returns>Массив байт представляющий дату в формате прибора.</returns>
        public static byte[] SetDateTime(DateTime dt)
        {
            byte[] byteArr = new byte[6];
            byteArr[1] = (byte)dt.Day;
            byteArr[0] = (byte)dt.Month;
            byteArr[3] = (byte)(dt.Year - 2000);
            byteArr[2] = (byte)dt.Hour;
            byteArr[5] = (byte)dt.Minute;
            byteArr[4] = (byte)dt.Second;

            return byteArr;
        }

        /// <summary>
        /// Увеличение даты на выбранный тип архива (месяц, день, час).
        /// </summary>
        /// <param name="dt"> Дата которая подлежит изменению.</param>
        /// <param name="typeArchive">Тип архива=интревал изменения.</param>
        /// <returns></returns>
        public static DateTime IncrementDate(DateTime dt, TypeArchive typeArchive)
        {
            DateTime retValue = DateTime.Now;
            if (typeArchive == TypeArchive.Day) { retValue = dt.AddDays(1); }
            if (typeArchive == TypeArchive.Mouth) { retValue = dt.AddMonths(1); }
            if (typeArchive == TypeArchive.Hour) { retValue = dt.AddHours(1); }

            return RoundDate(retValue,typeArchive);
        }

        /// <summary>
        /// Уменьшение даты на выбранный тип архива (месяц, день, час).
        /// </summary>
        /// <param name="dt"> Дата которая подлежит изменению.</param>
        /// <param name="typeArchive">Тип архива=интревал изменения.</param>
        /// <returns></returns>
        public static DateTime DecrementDate(DateTime dt, TypeArchive typeArchive)
        {
            DateTime retValue = DateTime.Now;
            if (typeArchive == TypeArchive.Day) { retValue= dt.AddDays(-1); }
            if (typeArchive == TypeArchive.Mouth) { retValue=dt.AddMonths(-1); }
            if (typeArchive == TypeArchive.Hour) { retValue=dt.AddHours(-1); }
            return RoundDate(retValue,typeArchive);
        }

        /// <summary>
        /// Округление даты до выбранного интервала (месяц, день, час).
        /// </summary>
        /// <param name="dt">Дата которая подлежит изменению.</param>
        /// <param name="typeArchive">Тип архива=интревал изменения.</param>
        /// <returns></returns>
        public static DateTime RoundDate(DateTime dt, TypeArchive typeArchive)
        {
            if (typeArchive == TypeArchive.Day) { return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0); }
            if (typeArchive == TypeArchive.Mouth) { return new DateTime(dt.Year, dt.Month, 1, 0, 0, 0); }
            if (typeArchive == TypeArchive.Hour) { return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0); }
            return dt;
        }
    }
}