﻿using System;

namespace TV7.core.Utils
{
    /// <summary>
    /// Класс для работы с типами данных приборов.
    /// </summary>
    internal static class Util
    {


        /// <summary>
        /// Класс преобразования получения Int32 из типа Int32 прибора (младший вперед).
        /// </summary>
        /// <param name="byteArray">Массив байт(ответ прибора).</param>
        /// <param name="startPosition">Позиция в массиве байт, с которой получаем Int32.</param>
        /// <returns></returns>
        public static int GetInt32(byte[] byteArray, int startPosition)
        {
            byte[] tmp = new byte[4];
            Array.Copy(byteArray, startPosition, tmp, 0, 4);
            Array.Reverse(tmp, 2, 2);
            Array.Reverse(tmp, 0, 2);
            return BitConverter.ToInt32(tmp, 0);
        }

        /// <summary>
        /// Класс преобразования получения Int16 из типа Int16 прибора (младший вперед).
        /// </summary>
        /// <param name="byteArray">Массив байт(ответ прибора).</param>
        /// <param name="startPosition">Позиция в массиве байт, с которой получаем Int16.</param>
        /// <returns></returns>
        public static int GetInt16(byte[] byteArray, int startPosition)
        {
            byte[] tmp = new byte[2];
            Array.Copy(byteArray, startPosition, tmp, 0, 2);
            Array.Reverse(tmp);
            return BitConverter.ToInt16(tmp, 0);
        }

        /// <summary>
        /// Класс преобразования получения Double из типа Float прибора (младший вперед).
        /// </summary>
        /// <param name="byteArray"></param>
        /// <param name="startPosition"></param>
        /// <returns></returns>
        public static double GetDouble(byte[] byteArray, int startPosition)
        {
            //byte[] tmp = new byte[4];
            //Array.Copy(byteArray, startPosition, tmp, 0, 4);
            //Array.Reverse(tmp, 2, 2);
            //Array.Reverse(tmp, 0, 2);
            return 0;// BitConverter.ToDouble(tmp, 0);
        }

        /// <summary>
        /// Переустановка байт в порядок младший вперед.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] GetReverseUshort(ushort value)
        {
            byte[] retValue = new byte[2];
            retValue = BitConverter.GetBytes(value);
            Array.Reverse(retValue);
            return retValue;
        }

    }
}