﻿using System;
using System.Collections.Generic;

namespace TV7.core.Utils
{
    internal static class BcdUtils
    {
        public static string GetStringFromBDC(byte value)
        {
            List<char> listArr = new List<char>();
            string retStr = string.Empty;
            byte lastDigit = (byte)(0b0000_1111 & value);
            byte firstDigit = (byte)((byte)(0b1111_0000 & value) >> 4);
            listArr.Add(Convert.ToChar(firstDigit + 0x30));
            listArr.Add(Convert.ToChar(lastDigit + 0x30));
            return new string(listArr.ToArray());
        }
    }
}