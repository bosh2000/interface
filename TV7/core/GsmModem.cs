﻿using System;
using System.Text;
using System.Threading;
using TV7.core.Log;

namespace TV7.core
{
    internal class GsmModem : IStream, IDisposable
    {
        /// <summary>
        /// COM порт , к которому подключен модем.
        /// </summary>
        private string GsmModemComPort;

        private string devicePhoneNumber;
        public bool isConnect { get; set; }

        private ComPort comPort;

        private Logger logger;

        public GsmModem(string DeviceComPort, string DevicePhoneNumber)
        {
            logger = Logger.GetInstance();
            this.GsmModemComPort = DeviceComPort;
            this.devicePhoneNumber = DevicePhoneNumber;
        }

        public void Dispose()
        {
            var sendString = "+++";
            comPort.WriteBytes(ASCIIEncoding.ASCII.GetBytes(sendString));

            Thread.Sleep(200);

            sendString = "ATH0" + "\r";
            comPort.WriteBytes(ASCIIEncoding.ASCII.GetBytes(sendString));
        }

        public byte[] GetResponce()
        {
            return comPort.GetResponce();
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Иницилизация модема, и набор номера.
        /// </summary>
        /// <returns></returns>
        public void Init()
        {
            try
            {
                logger?.Info($"Открытие порта - {GsmModemComPort} ");
                comPort = new ComPort(GsmModemComPort);
                comPort.Init();
                string dialString = "ATD" + devicePhoneNumber;
                logger?.Info($"Набор номера - {devicePhoneNumber}");
                // Установка количества повторов ожидания установки соединения. по умолчанию задержка в 1 сек.
                // обычно 30 сек установка соединения
                comPort.SetCount(30);
                comPort.WriteBytesNoAnswer(ASCIIEncoding.ASCII.GetBytes(dialString + "\r\n"));
                var count = 0;
                while (count != 30)
                {
                    Thread.Sleep(1000);
                    if (comPort.dataReceived)
                    {
                        string responceString = Encoding.UTF8.GetString(comPort.GetResponce());
                        logger?.Info($"Ответ модема {responceString}");
                        isConnect = IsErrorConnect(responceString);
                        break;
                    }
                    count++;
                };
            }
            catch (Exception exp)
            {
                logger?.Error($"Ошибка набора номера - {exp.Message}");
                throw new TV7DisconectException(exp.Message);
            }
        }

        /// <summary>
        /// Проверка статуса модемного подключения. если в ответе модема есть CONNECT то подключение считается успешным.
        /// Если в в ответе модема есть NO CARRIER , то считает что ошибка подключения.
        /// </summary>
        /// <param name="responceString">Строка ответа модема, при подключении.</param>
        /// <returns>Успешно или нет подключение.</returns>
        private bool IsErrorConnect(string responceString)
        {
            if (responceString.Contains("CONNECT"))
            {
                isConnect = true;
                logger?.Info("Подключение успешно.");
                return true;
            };
            if (responceString.Contains("NO CARRIER"))
            {
                logger?.Error("Ошибка подключения.");
                isConnect = false;
                return false;
            }
            return false;
        }

        public bool IsDatareceiverd()
        {
            return comPort.IsDatareceiverd();
        }

        public byte[] ReadBytes()
        {
            if (!isConnect) { throw new TV7DisconectException("Обрыв связи."); }
            return comPort.ReadBytes();
        }

        public Responce ReadResponce()
        {
            if (!isConnect) { throw new TV7DisconectException("Обрыв связи."); }
            return comPort.ReadResponce();
        }

        public void WriteBytes(byte[] bytes)
        {
            if (!isConnect) { throw new TV7DisconectException("Обрыв связи."); }
            try
            {
                comPort.WriteBytes(bytes);
            }
            catch (Tv7TimeOutException exp)
            {
                logger?.Error($"Ошибка опроса, {exp.Message}");
            }
        }

        public void WriteBytesNoAnswer(byte[] byteArr)
        {
            comPort.WriteBytesNoAnswer(byteArr);
        }

        public Request WriteRequest()
        {
            return comPort.WriteRequest();
        }
    }
}