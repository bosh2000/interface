﻿using System.Collections.Generic;
using System.Linq;

namespace TV7.core
{
    /// <summary>
    /// Класс реализующий тело запроса к прибору.
    /// </summary>
    internal class Request
    {
        private List<byte> Body;

        public Request()
        {
            Body = new List<byte>();
        }

        public void AddByteToBody(byte value)
        {
            Body.Add(value);
        }

        public void AddBytesToBody(byte[] value)
        {
            Body.AddRange(value);
        }

        public byte[] GetRequestBody()
        {
            return Body.ToArray<byte>();
        }

        public int GetLegthRequestBody()
        {
            return Body.Count<byte>();
        }

        public void Clear()
        {
            Body.Clear();
        }
    }
}