﻿using System.Collections.Generic;

namespace TV7.core
{
    /// <summary>
    /// Класс реализующий ответ прибора.
    /// </summary>
    internal class Responce
    {

        private List<byte> responceData;

        public Responce()
        {
            this.responceData = new List<byte>();
        }

        public byte[] GetResponceBody()
        {
            return responceData.ToArray();
        }

        public void SetResponceBody(byte[] byteArr)
        {
            responceData.Clear();
            responceData.InsertRange(0,byteArr);
        }

        public void Clear()
        {
            responceData.Clear();
        }
    }
}