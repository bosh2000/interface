﻿using System;
using System.Threading.Tasks;

namespace TV7.core
{
    /// <summary>
    /// Интерфейс Потока
    /// </summary>
    internal interface IStream : IDisposable
    {
        /// <summary>
        /// Инициализация потока.
        /// </summary>
        /// <returns></returns>
        void Init();

        /// <summary>
        /// Прочитать ответ прибора.
        /// </summary>
        /// <returns></returns>
        Responce ReadResponce();

        /// <summary>
        /// Послать запрос прибору.
        /// </summary>
        /// <returns></returns>
        Request WriteRequest();

        /// <summary>
        /// Прочитать ответ прибора в виде массива байт.
        /// </summary>
        /// <returns></returns>
        byte[] ReadBytes();

        /// <summary>
        /// Записать (послать) массив байт. с ожиданием ответа.
        /// </summary>
        /// <param name="bytes"></param>
        void WriteBytes(byte[] bytes);

        /// <summary>
        /// Записать (послать) массив байт, без ожидания ответа.
        /// </summary>
        /// <param name="byteArr"></param>
        /// <returns></returns>
        void WriteBytesNoAnswer(byte[] byteArr);

        /// <summary>
        /// Если данные  приняты, то True
        /// </summary>
        /// <returns></returns>
        bool IsDatareceiverd();

        /// <summary>
        /// Признак установки подключения.
        /// </summary>
        bool isConnect {  get;  set; }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        byte[] GetResponce();
    }
}