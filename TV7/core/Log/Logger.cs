﻿using NLog;
using System;
using System.Windows.Controls;
using System.Windows.Threading;

namespace TV7.core.Log
{
    internal class Logger
    {
        /// <summary>
        /// TextBox на главном окне для вывода отчетов.
        /// </summary>
        private TextBox LoggerArea;

        /// <summary>
        /// Экземпляр NLog.
        /// </summary>
        private static readonly NLog.Logger Log = LogManager.GetCurrentClassLogger();

        private static Logger instance;

        public static Logger GetInstance()
        {
            return instance;
        }

        /// <summary>
        /// Диспетчер потоков основного окна.
        /// </summary>
        private Dispatcher dispatcher;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger">TextBox для вывода сообщений.</param>
        public Logger(TextBox logger)
        {
            this.LoggerArea = logger;
            dispatcher.BeginInvoke(
                new Action(() =>
                    LoggerArea.Text = $"--- Начало логирования {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()} --- \n")
                    );
            Log.Trace(" --- Начала логирования ---");
            instance = this;
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger">TextBox для вывода сообщений.</param>
        /// <param name="dispatcher">Диспетчер потоков основного окна.</param>
        public Logger(TextBox logger, Dispatcher dispatcher)
        {
            this.LoggerArea = logger;
            this.dispatcher = dispatcher;
            dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = $"--- Начало логирования {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()} --- \n"));
            Log.Trace(" --- Начала логирования ---");
            instance = this;
        }

        /// <summary>
        /// Вывод сообщений с пометкой INFO
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = LoggerArea.Text + GetDateTime() + " INFO:" + message + "\n"));
            Log.Info(message);
        }

        /// <summary>
        /// Вывод массива байт в виде HEX строки. с направлением - отправка.
        /// </summary>
        /// <param name="bytes">Массив байт.</param>
        /// <param name="isLoggerArea">Флаг вывода дампа в экранную форму.</param>
        public void DumpSend(byte[] bytes, bool isLoggerArea)
        {
            string hexString = BytesToStringHEX(bytes);
            Log.Trace(">>>  " + hexString);
            if (isLoggerArea)
            {
                dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = LoggerArea.Text + GetDateTime() + " >>> " + hexString + "\n"));
            }
        }

        /// <summary>
        /// Вывод массива байт в виде HEX строки , с направлением - прием.
        /// </summary>
        /// <param name="bytes">Массив байт.</param>
        public void DumpReceive(byte[] bytes, bool isLoggerArea)
        {
            string hexString = BytesToStringHEX(bytes);
            Log.Trace("<<<  " + hexString);
            if (isLoggerArea)
            {
                dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = LoggerArea.Text + GetDateTime() + " <<< " + hexString + "\n"));
            }
        }

        /// <summary>
        /// Вывод сообщения с пометкой ERROR.
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = LoggerArea.Text + GetDateTime() + " ERROR:" + message + "\n"));
            Log.Debug(message);
        }

        /// <summary>
        /// Вывод сообщения с пометкой WARNINIG.
        /// </summary>
        /// <param name="message"></param>
        public void Warning(string message)
        {
            dispatcher.BeginInvoke(new Action(() => LoggerArea.Text = LoggerArea.Text + GetDateTime() + " WARNING:" + message + "\n"));
            Log.Warn(message);
        }

        /// <summary>
        /// Получение строкового представления даты/времени.
        /// </summary>
        /// <returns></returns>
        private string GetDateTime()
        {
            string retValue = string.Empty;
            DateTime dt = DateTime.Now;
            return retValue = dt.ToShortDateString() + " " + dt.ToLongTimeString();
        }

        /// <summary>
        /// Конвертирует массив байт в строку HEX
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        private string BytesToStringHEX(byte[] byteArray)
        {
            string hexString = string.Empty;
            foreach (byte value in byteArray)
            {
                hexString = hexString + string.Format("{0:X2} ", value);
            }
            return hexString;
        }
    }
}