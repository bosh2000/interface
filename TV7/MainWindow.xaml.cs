﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TV7.core;
using TV7.core.Log;
using TV7.core.Types;
using TV7.DB;
using TV7.DB.Entity;
using TV7.drivers;
using TV7.drivers.Aswega.Sa942;
using TV7.drivers.TbnEnergo.Kv5_4;
using TV7.drivers.Vzlet.TSRV024M;
using TV7.termotronic.tv7;

namespace TV7
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logger logger;
        private bool isProgress;
        private Thread processThread;
        private Thread errorWindowsThread;
        private Task pollService;
        private Db dataBase;

        public MainWindow()
        {
            InitializeComponent();
            logger = new Logger(LoggerArea, Dispatcher);
            isProgress = false;
            TelephoneNumberTextBox.IsEnabled = false;
            DataStartRead.DisplayDate = DateTime.Now;
            DataEndRead.DisplayDate = DateTime.Now;
            FillDeviceList();
            dataBase=new Db();
            SeedTest(dataBase);
            SerialTextBox.Text = string.Format("{0}", 0xF4BF87);
            // !!! переделать через Addrange
            string[] listComPort = SerialPort.GetPortNames();
            foreach (string value in listComPort)
            {
                ComPortSelectorComboBox.Items.Add(value);
            }
        }

        private void SeedTest(Db dataBase)
        {
            Device device = new Device();
            DeviceDriver drv=new DeviceDriver(){Driver = "TestDriver"};
            dataBase.Context.DeviceDriver.Add(drv);
            dataBase.Context.SaveChanges();

        }
        private async void ProcessedButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckDataForm();
                if (!isProgress)
                {
                    ReadDeviceData readDeviceData = new ReadDeviceData();
                    FullActiveReadType(readDeviceData);
                    //processThread = new Thread(new ParameterizedThreadStart(NewThreadReadData));
                    //processThread.SetApartmentState(ApartmentState.STA);
                    //processThread?.Start(readDeviceData);
                    pollService = NewThreadReadData(readDeviceData);

                    isProgress = true;
                    ProcessedButton.Content = "Остановить";
                }
                else
                {
                    //pollService.
                    // processThread?.Abort();
                    //isProgress = false;
                    ProcessedButton.Content = "Опросить";
                }
            }
            catch (Tv7StopInterviewWinthError exp)
            {
                isProgress = false;
                ProcessedButton.Content = "Опросить";
            }
        }

        private void FullActiveReadType(ReadDeviceData readDeviceData)
        {
            readDeviceData.dtStartRead = DataStartRead.DisplayDate;
            readDeviceData.dtEndRead = DataEndRead.DisplayDate;
            readDeviceData.serialNumber = int.Parse(SerialTextBox.Text);
            readDeviceData.adressDevice = int.Parse(AdressDeviceTextBox.Text);
            if ((bool)CurrentCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Current); }
            if ((bool)HourArchiveCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Hour); }
            if ((bool)DayArchiveCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Day); }
            if ((bool)MountArchiveCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Mouth); }
            if ((bool)ErrorArchiveCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Error); }
            if ((bool)IncidentArchiveCheckBox.IsChecked) { readDeviceData.ReadArchives.Add(TypeArchive.Incidents); }
        }

        private void CheckDataForm()
        {
        }

        private void Window_Closed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Запуск опроса в отдельном потоке.
        /// </summary>
        /// <param name="readDeviceData"></param>
        public async Task NewThreadReadData(object readDeviceData)
        {
            var rdd = (ReadDeviceData)readDeviceData;
            rdd.stream = GetDeviceStream();
            rdd.driver = GetDeviceDriver();
            rdd.serialNumber = Convert.ToInt32(SerialTextBox.Text);

            using (ReadDriver readDriver = new ReadDriver(logger, rdd))
            {
                try
                {
                    await readDriver.ReadData();   
                }
                catch (Exception exp)
                {
                    ErrorWindows errorWindows = new ErrorWindows();
                    errorWindows.ErrorWindowsTextBox.Text = $"Исключение:\n {exp.Message} \n\n {exp.StackTrace}";
                    errorWindows.ShowDialog();
                    isProgress = false;
                    logger.Error($"Исключение:\n {exp.Message} \n\n {exp.StackTrace}");
                    throw new Tv7StopInterviewWinthError(exp.Message);
                }
                logger.Info("Опрос закончен.");
            }
        }

        private IDrivers GetDeviceDriver()
        {
            string selectItem = (string)DeviceListComboBox.SelectedItem;

            switch (selectItem)
            {
                case "Sa-94/2":
                    return new Sa94_2();

                case "Tv - 7":
                    return new Tv7Driver();

                case "KM-5-4":
                    return new km5_4();
                case "ТРСВ-024М":
                    return new Tsrv024m();
                default:
                    return null;
            }
        }

        private IStream GetDeviceStream()
        {
            if ((bool)DirectConnectCheckBox.IsChecked)
            {
                string deviceComPort = (string)ComPortSelectorComboBox.SelectedItem;
                return new ComPort(deviceComPort);
            }
            if ((bool)UsingModemCheckBox.IsChecked)
            {
                string deviceComPort = (string)ComPortSelectorComboBox.SelectedItem;
                string devicePhoneNumber = (string)TelephoneNumberTextBox.Text;
                return new GsmModem(deviceComPort, devicePhoneNumber);
            }
            return null;
        }

        private void UsingModemCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)UsingModemCheckBox.IsChecked)
            {
                TelephoneNumberTextBox.IsEnabled = true;
            }
            else
            {
                TelephoneNumberTextBox.IsEnabled = false;
            }
        }

        private void UsingModemCheckBox_Click(object sender, RoutedEventArgs e)
        {
        }

        private void FillDeviceList()
        {
            DeviceListComboBox.Items.Add("Sa-94/2");
            DeviceListComboBox.Items.Add("Tv-7");
            DeviceListComboBox.Items.Add("KM-5-4");
            DeviceListComboBox.Items.Add("ТРСВ-024М");
        }
    }
}