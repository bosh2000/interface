﻿namespace TV7.DB.Entity
{
    public class DeviceDriver
    {
        public int Id { get; set; }
        public string Driver { get; set; }
    }
}