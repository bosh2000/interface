﻿using System.Data.Entity;

namespace TV7.DB.Entity
{

    public class IntAccounting : DbContext
    {
        public IntAccounting() : base("DefaultConnection")
        {
        }

        public DbSet<Device> Device { get; set; }

        public DbSet<DeviceDriver> DeviceDriver { get; set; }


    }
}