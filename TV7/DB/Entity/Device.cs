﻿namespace TV7.DB.Entity
{
    public class Device
    {
        public int Id { get; set; }

        public DeviceDriver Driver { get; set; }
    }
}