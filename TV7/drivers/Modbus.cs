﻿using System;
using System.Threading.Tasks;
using TV7.core;
using TV7.core.Log;
using TV7.core.Types;
using TV7.core.Utils;

namespace TV7.drivers
{
    /// <summary>
    /// Класс реализующий реализацию обмена по протоколу ModBus.
    /// </summary>
    internal partial class ModBus
    {
        private byte[] Request;

        /// <summary>
        /// Количество попыток.
        /// </summary>
        private int RetryCount = 5;

        /// <summary>
        /// Адрес устройства.
        /// </summary>
        private byte deviceAdress;

        /// <summary>
        /// Драйвер потока для обмена с прибором, реализующий интерфейс IStream.
        /// </summary>
        private IStream stream;

        /// <summary>
        /// Логгер.
        /// </summary>
        private Logger logger;

        /// <summary>
        /// Отладочная версия.
        /// </summary>
        private bool isDebug;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger">Экземляр логера.</param>
        /// <param name="stream">Драйвер потока.</param>
        public ModBus(IStream stream)
        {
            this.deviceAdress = 0x00;
            this.stream = stream;
            //this.logger = logger;
            logger = Logger.GetInstance();
            //#if DEBUG
            //            this.isDebug = true;
            //#else
            //            this.isDebug = false;
            //#endif
            this.isDebug = false;
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger">Экземляр логера.</param>
        /// <param name="stream">Драйвер потока.</param>
        /// <param name="adress">Сетевой адрес устроуства (ModBus).</param>
        public ModBus(Logger logger, IStream stream, byte adress)
        {
            this.deviceAdress = adress;
            this.stream = stream;
            this.logger = logger;
        }

        /// <summary>
        /// Перечисление функций ModBus.
        /// </summary>
        public enum CmdModBus
        {
            ReadHoldingRegister = 0x03,
            ReadInputRegister = 0x04,
            WriteAndReadHoldingRegisters = 0x48,
            WriteHoldingRegisters = 0x10,
        }

  
        /// Чтение регистра хранения.
        /// </summary>
        /// <param name="Register">Адрес регистра хранения.</param>
        /// <returns></returns>
        public Responce ReadHoldingRegister(ushort Register)
        {
            return ReadHoldingRegisters(Register, 1);
        }

        /// <summary>
        /// Чтение регистров хранения.
        /// </summary>
        /// <param name="startRegisters">Начальный регистр для чтения.</param>
        /// <param name="count">Количество регистров для чтения.</param>
        /// <returns></returns>
        public Responce ReadHoldingRegisters(ushort startRegisters, ushort count)
        {
            Request rqs = new Request();
            Responce rsp = new Responce();
            bool isRetry;
            int retryCount = RetryCount;
            do
            {
                isRetry = false;
                rsp.Clear();
                rqs.Clear();
                rqs.AddByteToBody(deviceAdress);
                rqs.AddByteToBody((byte)CmdModBus.ReadHoldingRegister);
                rqs.AddBytesToBody(Util.GetReverseUshort(startRegisters));
                rqs.AddBytesToBody(Util.GetReverseUshort(count));
                AddToResponceCrc16(rqs);
                logger.DumpSend(rqs.GetRequestBody(), isDebug);
                stream.WriteBytes(rqs.GetRequestBody());
                rsp.SetResponceBody(stream.GetResponce());
                logger.DumpReceive(rsp.GetResponceBody(), isDebug);
                try
                {
                    CheckCRC16(rsp);
                }
                catch (ErrorCRC16 exp)
                {
                    logger.Error(exp.Message);
                    isRetry = true;
                    retryCount--;
                }
            } while ((isRetry) && (retryCount == 0));
            if ((!isRetry) && retryCount == 0) { throw new EndModBusRetryCount("Ошибка чтения пакета, количество попыток чтения исчерпано."); }
            return CheckForErrorResponce(rsp);
        }

        /// <summary>
        /// Запись регистра хранения.
        /// </summary>
        /// <param name="register">Номер регистра для записи</param>
        /// <param name="bytes">Ushort для записи в регистр.</param>
        public void WriteHoldingRegister(ushort register, ushort bytes)
        {
            WriteHoldingRegisters(register, 1, BitConverter.GetBytes(bytes));
        }

        /// <summary>
        /// Запись несколько идущих подряд регистров хранения.
        /// </summary>
        /// <param name="startRegister">Начальный адрес регистра.</param>
        /// <param name="count">Число регистров для записи.</param>
        /// <param name="byteArr">Массив байт для записи.</param>
        public void WriteHoldingRegisters(ushort startRegister, ushort count, byte[] byteArr)
        {
            Request rqs = new Request();
            Responce rsp = new Responce();
            bool isRetry;
            int retryCount = RetryCount;
            do
            {
                isRetry = false;
                rqs.Clear();
                rsp.Clear();
                rqs.AddByteToBody(deviceAdress);
                rqs.AddByteToBody((byte)CmdModBus.WriteHoldingRegisters);
                rqs.AddBytesToBody(Util.GetReverseUshort(startRegister));
                rqs.AddBytesToBody(Util.GetReverseUshort(count));
                rqs.AddByteToBody((byte)byteArr.Length);
                rqs.AddBytesToBody(byteArr);
                AddToResponceCrc16(rqs);
                logger.DumpSend(rqs.GetRequestBody(), isDebug);
                stream.WriteBytes(rqs.GetRequestBody());

                rsp.SetResponceBody(stream.GetResponce());
                logger.DumpReceive(rsp.GetResponceBody(), isDebug);
                try
                {
                    CheckCRC16(rsp);
                }
                catch (ErrorCRC16 exp)
                {
                    logger.Error(exp.Message);
                    isRetry = true;
                    retryCount--;
                }
            } while ((isRetry) && (retryCount != 0));
            if ((!isRetry) && retryCount == 0) { throw new EndModBusRetryCount("Ошибка чтения пакета, количество попыток чтения исчерпано."); }
            CheckForErrorResponce(rsp);
        }

        /// <summary>
        /// Запись и чтение регистров, за один запрос.
        /// </summary>
        /// <param name="writeReadStruct">Структура с параметрами чтения и записи регистров.</param>
        /// <returns></returns>
        public Responce WriteAndReadRegisters(WriteReadStruct writeReadStruct)
        {
            Responce rsp = new Responce();
            Request rqs = new Request();
            bool isRetry;
            int retryCount = RetryCount;
            do
            {
                rsp.Clear();
                rqs.Clear();
                isRetry = false;
                rqs.AddByteToBody(deviceAdress);
                rqs.AddByteToBody((byte)CmdModBus.WriteAndReadHoldingRegisters);
                ///Начальный адрес регистра для чтения.
                rqs.AddBytesToBody(Util.GetReverseUshort(writeReadStruct.StartReadRegister));
                /// Количество регистров для чтения.
                rqs.AddBytesToBody(Util.GetReverseUshort(writeReadStruct.ReadCount));
                /// Начальный адрес регистра для записи.
                rqs.AddBytesToBody(Util.GetReverseUshort(writeReadStruct.StartWriteRegister));
                /// Количество регистров для записи.
                rqs.AddBytesToBody(Util.GetReverseUshort(writeReadStruct.WriteCount));
                /// Счетчик количество записываемых байт.
                rqs.AddBytesToBody(Util.GetReverseUshort((ushort)writeReadStruct.ToWriteByteArray.Length));
                /// Номер запроса.
                rqs.AddBytesToBody(new byte[] { 0x00, 0x01 });
                rqs.AddBytesToBody(writeReadStruct.ToWriteByteArray);
                AddToResponceCrc16(rqs);
                logger.DumpSend(rqs.GetRequestBody(), isDebug);
                stream.WriteBytes(rqs.GetRequestBody());
                rsp.SetResponceBody(stream.GetResponce());
                logger.DumpReceive(rsp.GetResponceBody(), isDebug);
                /// Проверка CRC16.
                try
                {
                    CheckCRC16(rsp);
                }
                catch (ErrorCRC16 exp)
                {
                    logger.Error(exp.Message);
                    isRetry = true;
                    retryCount--;
                }
                /// Проверка номера запроса 
                try
                {
                    CheckRequestNumber(rsp, rqs);
                } catch(DoNotMatchResponceNumber exp)
                {
                    logger.Error(exp.Message);
                    isRetry = true;
                    retryCount--;
                }
            } while (isRetry && (retryCount != 0));
            if ((!isRetry) && retryCount == 0)
            {
                throw new EndModBusRetryCount("Ошибка чтения пакета, количество попыток чтения исчерпано.");
            }
            return CheckForErrorResponce(rsp);
        }
        /// <summary>
        /// Проверка номера запроса для функции ModBus 0х48.
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="rqs"></param>
        private void CheckRequestNumber(Responce rsp,Request rqs)
        {

        }

        /// <summary>
        /// Запись нескольких идущих подряд регистров хранения, без ожидания ответа прибора.
        /// </summary>
        /// <param name="startRegister">Начальный адрес регистра.</param>
        /// <param name="count">Количество регистров для записи</param>
        /// <param name="byteArr">Массив байт для записи регистров.</param>
        public void WriteHoldingRegistersNoAnswer(ushort startRegister, ushort count, byte[] byteArr)
        {
            Request rqs = new Request();
            rqs.AddByteToBody(deviceAdress);
            rqs.AddByteToBody((byte)CmdModBus.WriteHoldingRegisters);
            rqs.AddBytesToBody(Util.GetReverseUshort(startRegister));
            rqs.AddBytesToBody(Util.GetReverseUshort(count));
            rqs.AddByteToBody((byte)byteArr.Length);
            rqs.AddBytesToBody(byteArr);
            logger.DumpSend(rqs.GetRequestBody(), isDebug);
            AddToResponceCrc16(rqs);
            stream.WriteBytesNoAnswer(rqs.GetRequestBody());
        }

        private void CheckCRC16(Responce rsp)
        {
            // Получаем размер сообщения ModBus - 2 байта CRC16.
            int lengthMessageBody = rsp.GetResponceBody().Length - 2;
            byte[] byteArray = new byte[lengthMessageBody];
            byte[] byteCRC16 = new byte[2];
            Array.Copy(rsp.GetResponceBody(), byteArray, lengthMessageBody);
            Array.Copy(rsp.GetResponceBody(), lengthMessageBody, byteCRC16, 0, 2);
            byte[] crc16 = CalculateCRC16(byteArray);

            if (BitConverter.ToInt16(byteCRC16, 0) != BitConverter.ToInt16(crc16, 0))
            {
                throw new ErrorCRC16("Не правильная контрольная сумма пакета.");
            }
        }

        /// <summary>
        /// Проверка ответа прибора на ошибки.
        /// В случае ошибки , генерируеться исключение по коду ошибки.
        /// </summary>
        /// <param name="rsp">Ответ прибора.</param>
        /// <returns></returns>
        private Responce CheckForErrorResponce(Responce rsp)
        {
            if ((rsp != null) && (rsp.GetResponceBody().Length != 0) && ((rsp.GetResponceBody()[1] & 0b10000000) == 0b10000000))
            {
                if ((rsp.GetResponceBody()[1] == 0x83) || (rsp.GetResponceBody()[1] == 0x90))
                {
                    byte errorCode = rsp.GetResponceBody()[2];
                    switch (errorCode)
                    {
                        case 0x01:
                            throw new FaultFunctionNumberException("Недопустимая функция.");
                        case 0x02:
                            throw new FaultAdressRegisterException("Недопустимый адрес регистра в запросе.");
                        case 0x03:
                            throw new FaultInDataRequest("Недопустимые данные в теле запроса.");
                        case 0x04:
                            throw new FatalRunTimeErrorModBusException("Непопровимая ошибка во время выполнения операции.");
                        case 0x06:
                            throw new RunTimeErrorTryLaterException("Запрос не может быть обработан сейчас, необходимо повторить запрос позднее.");
                        case 0x09:
                            throw new DeviceBusyException("Устройство не готово.");
                        case 0x0A:
                            throw new ReadMoreRegistersThatAllowedException("При чтении запрошенно больше регистров, чем допустимо.");
                        case 0x0B:
                            throw new WriteMoreRegistersThatAllowedException("Попытка записи большего количество регистров, чем допустимо.");
                        case 0x0C:
                            throw new FaultStartAdressException("Недопустимый начальный адрес регистра.");
                        case 0X0D:
                            throw new FaultEndAdressException("Недопустимый конечный адрес регистра.");
                        case 0x0E:
                            throw new AdressOnlyReadException("Адрес регистра только для чтения.");
                        case 0x0F:
                            throw new AccessDeniedException("Доступ запрещен.");
                        case 0x10:
                            throw new WrongErrorRunTimeException("Неизвестная ошибка выполнения запроса.");
                        case 0x82:
                            throw new RunTimeErrorException("Ошибка выполнения запроса.");
                        case 0x84:
                            throw new DateOutOfRangeException("Указанная дата вне диапазона архива пробора.");
                        case 0x85:
                            throw new NoDataSpecifiedDate("Нет данных за указанную дату.");
                    }
                }
            }
            return rsp;
        }

        /// <summary>
        /// Подсчет и добавленеие к телу запроса контрольной суммы CRC16.
        /// </summary>
        /// <param name="request">Тело запроса.</param>
        public void AddToResponceCrc16(Request request)
        {
            request.AddBytesToBody(CalculateCRC16(request.GetRequestBody()));
        }

        /// <summary>
        /// Подсчет контрольной суммы CRC16.
        /// </summary>
        /// <param name="buf">Массив байт для подсчета CRC16</param>
        /// <returns>CRC16</returns>
        public byte[] CalculateCRC16(byte[] buf)
        {
            int len = buf.Length;
            UInt16 crc = 0xFFFF;
            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (UInt16)buf[pos];
                for (int i = 8; i != 0; i--)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc >>= 1;
                        crc ^= 0xA001;
                    }
                    else
                        crc >>= 1;
                }
            }
            return BitConverter.GetBytes(crc);
        }
    }
}