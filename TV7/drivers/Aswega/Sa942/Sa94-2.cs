﻿using System;
using System.Threading;
using TV7.core;
using TV7.core.Log;
using TV7.core.Types;
using TV7.core.Utils;

namespace TV7.drivers.Aswega.Sa942
{
    internal class Sa94_2 : IDrivers
    {
        private ReadDeviceData rdd;
        private Logger logger;

        public void Dispose()
        {
            rdd.stream.Dispose();
        }

        public void Init(ReadDeviceData readDeviceData)
        {
            this.rdd = readDeviceData;
            this.rdd.stream.Init();
            logger = Logger.GetInstance();
            //TODO переделать на исключения.
            if (!readDeviceData.stream.isConnect)
            {
                throw new Tv7StopInterviewWinthError("Ошибка инициализации потока.");
            };

            rdd.stream.WriteBytes(new byte[] { 0xc0, 0x50, 0x3B });
            byte[] stateDevice = rdd.stream.GetResponce();
            if ((byte)(0b0000_0001 & stateDevice[0]) == (byte)0b1000_0001)
            {
                logger.Info($"SA-94 - Температура Т3 программируется.");
            }
            else
                logger.Info($"SA-94 - Температура Т3 измеряется.");

            if ((byte)(0b0000_0010 & stateDevice[0]) == (byte)0b0000_0010)
            {
                logger.Info($"SA-94 - В расчетах применяются Т1, Т2, Т3.");
            }
            else
                logger.Info($"SA-94 - В расчетах применяются Т1, Т2.");

            if ((byte)(0b0000_0100 & stateDevice[0]) == (byte)0b0000_0100)
            {
                logger.Info($"SA-94 - модель теплосчетчика SA-94/2.");
            }

            if ((byte)(0b0001_0000 & stateDevice[0]) == (byte)0b0001_0000)
            {
                logger.Info($"SA-94 - Теплосчетчик в режиме \"Счет\".");
            }
            else
                logger.Info($"SA-94 - Теплосчетчик в режиме \"Стоп\".");

            if ((byte)(0b1000_0000 & stateDevice[0]) == (byte)0b1000_0000)
            {
                logger.Info($"SA-94 - Теплосчетчик в режиме автокалибровки.");
            }
            else
                logger.Info($"SA-94 - Теплосчетчик в режиме ручной калибровки.");
        }

        public void ReadArchiveData(ArchiveHeader archiveHeader)
        {
            throw new Tv7NotImplemented(" Чтение архивных данных, Не реализованно.");
        }

        public ArchiveHeader ReadArchiveHeader()
        {
            throw new Tv7NotImplemented("Чтение заголовка архивов.Не реализованно.");
        }

        public void ReadConfig()
        {
            throw new Tv7NotImplemented("Чтение конфигурации , Не реализованно.");
            Thread.Sleep(1000);
        }

        public void ReadCurrentData()
        {
            rdd.stream.WriteBytes(new byte[] { 0b1000_0000 });
            byte[] responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано Q1 - расход теплоносителя в прямом или обратном трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0001 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано Q2 - расход теплоносителя  в обратном или третьем трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0010 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано T1 - температура теплоносителя в прямом трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0011 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано T2 - температура теплоносителя в обратном трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0100 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано T3 - температура теплоносителя в третьем трубопроводе или наружного воздуха");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0101 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано dT(Т1-Т2) - разность температур теплоносителя");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0110 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано P - потребляемая тепловая мощность");

            rdd.stream.WriteBytes(new byte[] { 0b1000_0111 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано E - количество теплоты");

            rdd.stream.WriteBytes(new byte[] { 0b1000_1000 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано V1 - масса теплоносителя  в  прямом или обратном трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_1001 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано V2 - масса теплоносителя  обратном или третьем трубопроводе");

            rdd.stream.WriteBytes(new byte[] { 0b1000_1100 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано Время работы теплосчетчика в режиме <Работа> и <Счет>");

            rdd.stream.WriteBytes(new byte[] { 0b1000_1110 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано р1  - давление в первом канале измерения");

            rdd.stream.WriteBytes(new byte[] { 0b1000_1111 });
            responceDevice = rdd.stream.GetResponce();
            logger.Info($"Считано р2  - давление во втором канале измерения");
        }

        public void ReadDateTime()
        {
            rdd.stream.WriteBytes(new byte[] { 0b1000_1010 });
            byte[] timeDevice = rdd.stream.GetResponce();
            rdd.stream.WriteBytes(new byte[] { 0b1000_1011 });
            byte[] dateDevice = rdd.stream.GetResponce();
            DateTime dt = GetDateTime(timeDevice, dateDevice);
            logger.Info($"Дата и время по данным устройства - {dt.ToShortDateString()} - { dt.ToLongTimeString()}");
        }

        private DateTime GetDateTime(byte[] timeDevice, byte[] dateDevice)
        {
            int year = Convert.ToInt32(BcdUtils.GetStringFromBDC(dateDevice[3]));
            int mounth = Convert.ToInt32(BcdUtils.GetStringFromBDC(dateDevice[2]));
            int day = Convert.ToInt32(BcdUtils.GetStringFromBDC(dateDevice[1]));
            int hour = Convert.ToInt32(BcdUtils.GetStringFromBDC(timeDevice[1]));
            int min = Convert.ToInt32(BcdUtils.GetStringFromBDC(timeDevice[2]));
            int sec = Convert.ToInt32(BcdUtils.GetStringFromBDC(timeDevice[3]));
            return new DateTime(year, mounth, day, hour, min, sec);
        }

        public void SetDateTime(DateTime dt)
        {
            throw new Tv7NotImplemented("Установка даты и времени прибора.Не реализованно.");
        }

        #region private

        private byte[] GenerateSerialNumber(int serialNumber)
        {
            byte[] resultByte = new byte[3];
            byte[] serialNumberArray = BitConverter.GetBytes(serialNumber);

            resultByte[2] = (byte)(0x7F & serialNumberArray[3]);

            return resultByte;
        }

        #endregion private
    }
}