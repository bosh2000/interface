﻿using System;

namespace TV7.drivers
{
    /// <summary>
    /// Общий класс исключений для ModBus.
    /// </summary>
    internal class ModBusExceptions : Exception
    {
        public ModBusExceptions(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Не совпадает номер запроса и номер ответа.
    /// </summary>
    internal class DoNotMatchResponceNumber : ModBusExceptions
    {
        public DoNotMatchResponceNumber(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Исчерпаны попытки чтения.
    /// </summary>

    internal class EndModBusRetryCount : ModBusExceptions
    {
        public EndModBusRetryCount(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Ошибка контрольной суммы.
    /// </summary>
    internal class ErrorCRC16 : ModBusExceptions
    {
        public ErrorCRC16(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, недопустимая фукнция.
    /// </summary>
    internal class FaultFunctionNumberException : ModBusExceptions
    {
        public FaultFunctionNumberException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, недопустимый адрес регистра в запросе.
    /// </summary>
    internal class FaultAdressRegisterException : ModBusExceptions
    {
        public FaultAdressRegisterException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// ИСключение. Недопустимое значение в поле данных запроса.
    /// </summary>
    internal class FaultInDataRequest : ModBusExceptions
    {
        public FaultInDataRequest(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, непоправимая ошибка во время выполнения операции.
    /// </summary>
    internal class FatalRunTimeErrorModBusException : ModBusExceptions
    {
        public FatalRunTimeErrorModBusException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// ИСключение, запрос не может быть обработан сейчас, необходимо повторить запрос позднее.
    /// </summary>
    internal class RunTimeErrorTryLaterException : ModBusExceptions
    {
        public RunTimeErrorTryLaterException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Устройство занято.
    /// </summary>
    internal class DeviceBusyException : ModBusExceptions
    {
        public DeviceBusyException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Запрошенно больше регистров, чем допустимо.
    /// </summary>
    internal class ReadMoreRegistersThatAllowedException : ModBusExceptions
    {
        public ReadMoreRegistersThatAllowedException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// ИСключение, Попытка записи больше регистров, чем допустимо.
    /// </summary>
    internal class WriteMoreRegistersThatAllowedException : ModBusExceptions
    {
        public WriteMoreRegistersThatAllowedException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Ошибочный начальный адрес.
    /// </summary>
    internal class FaultStartAdressException : ModBusExceptions
    {
        public FaultStartAdressException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Ошибочный конечный адрес.
    /// </summary>
    internal class FaultEndAdressException : ModBusExceptions
    {
        public FaultEndAdressException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Адрес только для чтения.
    /// </summary>
    internal class AdressOnlyReadException : ModBusExceptions
    {
        public AdressOnlyReadException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение. Доступ запрещен.
    /// </summary>
    internal class AccessDeniedException : ModBusExceptions
    {
        public AccessDeniedException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение. Ошибка выполнения запроса.
    /// </summary>
    internal class WrongErrorRunTimeException : ModBusExceptions
    {
        public WrongErrorRunTimeException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение. Ошибка выполнения.
    /// </summary>
    internal class RunTimeErrorException : ModBusExceptions
    {
        public RunTimeErrorException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение, Дата вне диапазона прибора.
    /// </summary>
    internal class DateOutOfRangeException : ModBusExceptions
    {
        public DateOutOfRangeException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Исключение,  нет данных на указанную дату.
    /// </summary>
    internal class NoDataSpecifiedDate : ModBusExceptions
    {
        public NoDataSpecifiedDate(string message) : base(message)
        {
        }
    }
}