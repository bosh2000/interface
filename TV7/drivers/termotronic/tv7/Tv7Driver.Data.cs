﻿namespace TV7.drivers.termotronic.tv7
{
    partial class Tv7Driver
    {
        private enum Register
        {
            TypeDevice = 0x0000,
            Version = 0x0001,
            HardVersion = 0x0002,
            CRCSoftware = 0x0003,
            SerialNumber = 0x0005
        }
    }
}