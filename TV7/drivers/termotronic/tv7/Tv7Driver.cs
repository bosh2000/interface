﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TV7.core;
using TV7.core.Log;
using TV7.core.Types;
using TV7.core.Utils;
using TV7.drivers;

namespace TV7.termotronic.tv7
{
    internal class Tv7Driver : IDrivers
    {
        #region Переменные

        /// <summary>
        /// Логер.
        /// </summary>
        private Logger logger;

        /// <summary>
        /// Экземпляр класса реализующего ModBus.
        /// </summary>
        private ModBus modBus;

        private ReadDeviceData readDeviceData;

        #endregion Переменные

        public Tv7Driver()
        {
            this.logger = Logger.GetInstance() ;
        }

        #region Init

        public void Init(ReadDeviceData readDeviceData)
        {
         //   await Task.Run(() =>
           // {
                this.readDeviceData = readDeviceData;
                readDeviceData?.stream.Init();
                modBus = new ModBus(readDeviceData.stream);
            //});
        }

        #endregion Init

        #region SetDateTime

        public void SetDateTime(DateTime dt)
        {
            //await Task.Run(() =>
            //{
            //    throw new Tv7NotImplemented("Установка даты\\времени не реализованно.");
            //});
        }

        #endregion SetDateTime

        #region ReadConfig

        public void ReadConfig()
        {
            Responce rsp = new Responce();
            rsp = modBus.ReadHoldingRegister(0x0000);
            logger.Info(string.Format("Тип устройства -{0:X4}", BitConverter.ToInt16(rsp.GetResponceBody(), 3)));
            rsp = modBus.ReadHoldingRegister(0x0001);
            logger.Info(string.Format("Версия ПО -{0:X4}", BitConverter.ToInt16(rsp.GetResponceBody(), 3)));
            rsp = modBus.ReadHoldingRegisters(0x0005, 2);
            logger.Info($"Серийный номер -{Util.GetInt32(rsp.GetResponceBody(), 3)}");
            rsp = modBus.ReadHoldingRegister(0x0869);
            logger.Info($"Сетевой адрес прибора- {Util.GetInt16(rsp.GetResponceBody(), 3)}");
            rsp = modBus.ReadHoldingRegister(0x69);
            readDeviceData.reportingDateTime.ReportingDay = rsp.GetResponceBody()[0];
            readDeviceData.reportingDateTime.ReportingHour = rsp.GetResponceBody()[1];
            logger.Info($"Отчетный час -{readDeviceData.reportingDateTime.ReportingHour}");
            logger.Info($"Отчетный день - {readDeviceData.reportingDateTime.ReportingDay}");
        }

        #endregion ReadConfig

        #region ReadArchiveHeader

        /// <summary>
        /// Чтение заголовков архивов.
        /// </summary>
        /// <returns></returns>
        public ArchiveHeader ReadArchiveHeader()
        {
            ArchiveHeader archiveHeader = new ArchiveHeader();
            Responce rsp = new Responce();
            logger.Info("Информация о датах начала\\конца архивов.");
            /// Чтение заголовка часового архива.
            ArchiveDateDistance aDDHour = new ArchiveDateDistance();
            aDDHour.TypeArchive = TypeArchive.Hour;
            rsp = modBus.ReadHoldingRegisters(0x0a74, 3);
            aDDHour.startDateTime = DateUtils.GetDateTime(rsp, 3);
            rsp =  modBus.ReadHoldingRegisters(0x0a80, 3);
            aDDHour.endDateTime = DateUtils.GetDateTime(rsp, 3);
            logger.Info(string.Format("Интервал часового архива : {0} - {1}", aDDHour.startDateTime, aDDHour.endDateTime));
            archiveHeader.AddArchiveDateDistance(aDDHour);

            /// Чтение заголовка суточного архива.
            ArchiveDateDistance aDDDay = new ArchiveDateDistance();
            aDDDay.TypeArchive = TypeArchive.Day;
            rsp =  modBus.ReadHoldingRegisters(0x0A77, 3);
            aDDDay.startDateTime = DateUtils.GetDateTime(rsp, 3);
            rsp =  modBus.ReadHoldingRegisters(0x0A83, 3);
            aDDDay.endDateTime = DateUtils.GetDateTime(rsp, 3);
            logger.Info(string.Format("Интервал суточного архива : {0} - {1}", aDDDay.startDateTime, aDDDay.endDateTime));
            archiveHeader.AddArchiveDateDistance(aDDDay);

            /// Чтение заголовка месячного архива.
            ArchiveDateDistance aDDMouth = new ArchiveDateDistance();
            aDDMouth.TypeArchive = TypeArchive.Mouth;
            rsp =  modBus.ReadHoldingRegisters(0x0A7A, 3);
            aDDMouth.startDateTime = DateUtils.GetDateTime(rsp, 3);
            rsp =  modBus.ReadHoldingRegisters(0x0A86, 3);
            aDDMouth.endDateTime = DateUtils.GetDateTime(rsp, 3);
            logger.Info(string.Format("Интервал месячного архива : {0} - {1}", aDDMouth.startDateTime, aDDMouth.endDateTime));

            //ArchiveDateDistance aDDIntegral = new ArchiveDateDistance();

            //rsp = await modBus.ReadHoldingRegisters(0x0A7d, 3);
            //dtStartArchive = DateUtils.GetDateTime(rsp, 3);
            //rsp = await modBus.ReadHoldingRegisters(0x0A89, 3);
            //dtStopArchive = DateUtils.GetDateTime(rsp, 3);
            //logger.Info(string.Format("Интервал итого архива : {0} - {1}", dtStartArchive, dtStopArchive));

            return archiveHeader;
        }

        #endregion ReadArchiveHeader

        #region ReadArchiveData

        /// <summary>
        /// Чтение архивных записей прибора.
        /// </summary>
        /// <returns></returns>
        public  void ReadArchiveData(ArchiveHeader archiveHeader)
        {
            DateTime dtStartRead = readDeviceData.dtStartRead;
            DateTime dtEndRead = readDeviceData.dtEndRead;
            foreach (var typeArchive in readDeviceData.ReadArchives)
            {
                foreach (var archiveHeaderItem in archiveHeader)
                {
                    if (archiveHeaderItem.TypeArchive == typeArchive)
                    {
                        if (dtStartRead < DateUtils.RoundDate(archiveHeaderItem.startDateTime, typeArchive))
                        {
                            logger.Info($"Начальная дата {dtStartRead} чтения архива меньше даты " +
                                $"{DateUtils.RoundDate(archiveHeaderItem.startDateTime, typeArchive)} начала архива прибора. Начало чтение установленно в начало архива прибора.");
                            dtStartRead = DateUtils.RoundDate(archiveHeaderItem.startDateTime, typeArchive);
                        }
                        if (dtEndRead > DateUtils.RoundDate(archiveHeaderItem.endDateTime, typeArchive))
                        {
                            logger.Info($"Конечная дата {dtEndRead} чтения архива больше даты " +
                                $"{DateUtils.RoundDate(archiveHeaderItem.endDateTime, typeArchive)} окончания архива прибора. Дата окончания чтение установленно в конец архива прибора.");
                            dtEndRead = DateUtils.RoundDate(archiveHeaderItem.endDateTime, typeArchive);
                        }
                    }
                }
                for (var indexDate = DateUtils.RoundDate(dtStartRead, typeArchive); indexDate <= dtEndRead;)
                {
                    logger.Info($"Читаемая дата архива - {indexDate}");
                    // запрос к прибору , установка данных для чтения архива.
                    List<byte> byteArr = new List<byte>();
                    WriteReadStruct writeReadStruct;
                    writeReadStruct.StartReadRegister = 0x0AB4;
                    writeReadStruct.ReadCount = 0x006D;   // Штатная программа читает вроде больше. проверить;
                    writeReadStruct.StartWriteRegister = 0x0063;
                    writeReadStruct.WriteCount = 0x0006;
                    // Заполняем дамп регистров для записи
                    byteArr.AddRange(DateUtils.SetDateTime(indexDate));
                    byteArr.AddRange(Util.GetReverseUshort((ushort)typeArchive));
                    // Номер записи , для чтения асинхронных архивов. Резерв 1.
                    byteArr.AddRange(new byte[] { 0x00, 0x00, 0x00, 0x00 });
                    writeReadStruct.ToWriteByteArray = byteArr.ToArray();
                    Responce rsp = modBus.WriteAndReadRegisters(writeReadStruct);
                    indexDate = DateUtils.IncrementDate(indexDate, typeArchive);
                }
            }
        }

        #endregion ReadArchiveData

        #region ReadCurrentData

        /// <summary>
        /// Чтение Текущих данных прибора.
        /// </summary>
        /// <returns></returns>
        public void ReadCurrentData()
        {
            ReadDateTime();
            Responce rsp = new Responce();
            rsp = modBus.ReadHoldingRegisters(0x0DD7, 12);
            logger.Info(string.Format("Температура t{0} - {1}", 1, Util.GetDouble(rsp.GetResponceBody(), 3)));
            logger.Info(string.Format("Температура t{0} - {1}", 2, Util.GetDouble(rsp.GetResponceBody(), 7)));
            logger.Info(string.Format("Температура t{0} - {1}", 3, Util.GetDouble(rsp.GetResponceBody(), 11)));
            logger.Info(string.Format("Температура t{0} - {1}", 4, Util.GetDouble(rsp.GetResponceBody(), 15)));
            logger.Info(string.Format("Температура t{0} - {1}", 5, Util.GetDouble(rsp.GetResponceBody(), 19)));
            //  logger.Info(string.Format("Температура t{0} - {1}", 6, GetDouble(rsp.ResponceData, 23)));
        }

        #endregion ReadCurrentData

        #region ReadDateTime

        /// <summary>
        /// Чтение даты и времени прибора.
        /// </summary>
        /// <returns></returns>
        public void ReadDateTime()
        {
            Responce rsp = new Responce();
            rsp = modBus.ReadHoldingRegisters(0x0DD4, 3);
            DateTime currentDateTime = DateUtils.GetDateTime(rsp, 3);
            logger.Info(string.Format("Текущая дата прибора - {0}  {1}", currentDateTime.ToShortDateString(), currentDateTime.ToShortTimeString()));
        }

        #endregion ReadDateTime

        /// <summary>
        /// Реализация интерфейса IDisposable
        /// </summary>
        public void Dispose()
        {
            readDeviceData.stream.Dispose();
        }
    }
}