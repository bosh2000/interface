﻿using System;
using System.Threading.Tasks;
using TV7.core.Types;

namespace TV7.drivers
{
    /// <summary>
    /// Интерфейс драйвера.
    /// </summary>
    internal interface IDrivers : IDisposable
    {
        /// <summary>
        /// Инициализация драйвера.
        /// </summary>
        void Init(ReadDeviceData readDeviceData);

        /// <summary>
        /// Чтение конфигурации прибора.
        /// </summary>
        void ReadConfig();

        /// <summary>
        /// Чтение текущий данных прибора.
        /// </summary>
        void ReadCurrentData();

        /// <summary>
        /// Чтение текущие время и дату прибора.
        /// </summary>
        void ReadDateTime();

        /// <summary>
        /// Установка даты и времени прибора.
        /// </summary>
        void SetDateTime(DateTime dt);

        /// <summary>
        /// Чтение заголовков архивов.
        /// </summary>
        ArchiveHeader ReadArchiveHeader();

        /// <summary>
        /// Чтение архивных данных.
        /// </summary>
        void ReadArchiveData(ArchiveHeader archiveHeader);
    }
}