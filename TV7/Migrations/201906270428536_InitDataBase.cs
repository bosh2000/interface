namespace TV7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDataBase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Driver_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviceDrivers", t => t.Driver_Id)
                .Index(t => t.Driver_Id);
            
            CreateTable(
                "dbo.DeviceDrivers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Driver = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Devices", "Driver_Id", "dbo.DeviceDrivers");
            DropIndex("dbo.Devices", new[] { "Driver_Id" });
            DropTable("dbo.DeviceDrivers");
            DropTable("dbo.Devices");
        }
    }
}
